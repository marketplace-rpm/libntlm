# Information / Информация

SPEC-файл для создания RPM-пакета **libntlm**.

## Install / Установка

1. Подключить репозиторий **MARKETPLACE**: `dnf copr enable marketplace/libs`.
2. Установить пакет: `dnf install libntlm`.

## Donation / Пожертвование

- [Donating](https://donating.gitlab.io/)